package com.starlight.mvexmpl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MvExmplApplication {

	public static void main(String[] args) {
		SpringApplication.run(MvExmplApplication.class, args);
	}

}
