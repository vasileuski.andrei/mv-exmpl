package com.starlight.mvexmpl.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hi")
public class CommonController {

    @GetMapping
    public String getCurrentWeatherInfo() {

        return "weatherService.getCurrentWeatherInfo()!!";
    }


}
