FROM khipu/openjdk17-alpine
COPY target/mv-exmpl-0.0.1-SNAPSHOT.jar /mv-exmpl.jar
CMD ["java", "-jar", "/mv-exmpl.jar"]